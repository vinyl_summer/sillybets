FROM python:3.10-buster
ENV PYTHONBUFFERED=1
RUN mkdir /app
COPY ./requirements.txt /app/requirements.txt
WORKDIR /app
RUN pip install -r requirements.txt
COPY . /app
CMD python main.py
