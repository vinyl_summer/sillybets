import logging
import os
from datetime import datetime, timedelta

from telegram import Update
from telegram.ext import ApplicationBuilder, ContextTypes, CommandHandler

from models import User, Event, Bet

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO
)

assert "API_TOKEN" in os.environ


async def start(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text="This silly bot will help you bet on an outcome of silly events."
             " Use /me to check your account, /bet to check out things to bet on,"
             " /create to create your own events and /end to end your events")
    new_user = User(tg_id=update.effective_user.id, name=update.effective_user.full_name)
    new_user.save()


async def create_event(update: Update, context: ContextTypes.DEFAULT_TYPE):
    try:
        name, duration = update.message.text.split()[1:]
        new_event = Event(
            creator=User.get(tg_id=update.effective_user.id),
            name=name,
            betting_ends_at=datetime.now() + timedelta(minutes=int(duration)))
        new_event.save()
        await context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=f"Successfully created event named {new_event.name}!")
    except (IndexError, ValueError):
        await update.message.reply_text("Usage: /create <event_name> <betting_duration in minutes>")


async def create_bet(update: Update, context: ContextTypes.DEFAULT_TYPE):
    try:
        name, outcome, bet_amount = update.message.text.split()[1:]

    except (IndexError, ValueError):
        # current_events = "\n".join(f"{event.get_id()}) {event.name} by {event.creator.name}"
        #                            for event in Event.select().where(Event.betting_ends_at < datetime.now()))
        current_events = "\n".join(f"{event.get_id()}) {event.name} by {event.creator.name}"
                                   for event in Event.select())
        await context.bot.send_message(
            chat_id=update.effective_chat.id,
            text="List of currently running events:\n" + current_events
        )
        await update.message.reply_text("Usage: /bet <event_name> <outcome, either W or L> <amount>")
        return

    user = User.get(tg_id=update.effective_user.id)

    if int(bet_amount) > user.balance:
        await update.message.reply_text("Sorry, you don't have enough vbucks!")
        return

    event = Event.get_or_none(name=name)
    if event is None:
        await update.message.reply_text("Sorry, could not find an event with that name!")
        return
    if outcome not in ["W", "L"]:
        await update.message.reply_text("Usage: /bet <event_name> <outcome, either W or L> <amount>")
        return
    if event.betting_ends_at < datetime.now():
        await update.message.reply_text("Sorry, betting on that event already ended!")
        return
    if not event.ongoing:
        await update.message.reply_text("Sorry, that event already ended!")
        return

    user = User.get(tg_id=update.effective_user.id)
    user.balance -= int(bet_amount)
    user.save()

    new_bet = Bet(
        user=user,
        event=event,
        outcome=str(outcome),
        amount=int(bet_amount))
    new_bet.save()

    await update.message.reply_text(f"Successfully created a {new_bet.amount} vbucks bet on {event.name} event")


async def end_event(update: Update, context: ContextTypes.DEFAULT_TYPE):
    try:
        name, result = update.message.text.split()[1:]
        if result not in ["W", "L"]:
            raise ValueError
    except (IndexError, ValueError):
        await update.message.reply_text("Usage: /end <event_name> <outcome, either W or L>")
        return

    event = Event.get_or_none(name=name)
    if event is None:
        await update.message.reply_text("Sorry, could not find an event with that name")
        return
    if result == "W":
        winning_bets = Bet.select().where((Bet.event == event) & (Bet.outcome == "W"))
        losing_bets = Bet.select().where((Bet.event == event) & (Bet.outcome == "L"))
    elif result == "L":
        winning_bets = Bet.select().where((Bet.event == event) & (Bet.outcome == "W"))
        losing_bets = Bet.select().where((Bet.event == event) & (Bet.outcome == "L"))

    for bet in winning_bets:
        bet.user.balance += bet.amount * 2
        await context.bot.send_message(
            chat_id=bet.user.tg_id,
            text=f"You have won {bet.amount} vbucks from {event.name} event")
        bet.user.save()
    for bet in losing_bets:
        await context.bot.send_message(
            chat_id=bet.user.tg_id,
            text=f"You have lost {bet.amount} vbucks from {event.name} event")

    await update.message.reply_text("Ended the game, successfully awarded points to the winners")

    event.delete_instance()


async def show_stats(update: Update, context: ContextTypes.DEFAULT_TYPE):
    user = User.get(tg_id=update.effective_user.id)
    await update.message.reply_text(f"Your name: {user.name}\nYour balance: {user.balance} vbucks")

if __name__ == '__main__':
    application = ApplicationBuilder().token(os.getenv("API_TOKEN")).build()

    application.add_handler(CommandHandler('start', start))
    application.add_handler(CommandHandler('create', create_event))
    application.add_handler(CommandHandler('bet', create_bet))
    application.add_handler(CommandHandler('end', end_event))
    application.add_handler(CommandHandler('me', show_stats))

    application.run_polling()
