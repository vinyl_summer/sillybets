from peewee import (SqliteDatabase, Model,
                    TextField, DateTimeField,
                    ForeignKeyField, BooleanField, IntegerField)

db = SqliteDatabase('users.db')


class BaseModel(Model):
    class Meta:
        database = db


class User(BaseModel):
    name = TextField()
    tg_id = TextField(unique=True)
    balance = IntegerField(default=1000)


class Event(BaseModel):
    creator = ForeignKeyField(User, on_delete='CASCADE')
    name = TextField()
    ongoing = BooleanField(default=True)
    betting_ends_at = DateTimeField()


class Bet(BaseModel):
    user = ForeignKeyField(User, on_delete='CASCADE')
    event = ForeignKeyField(Event, on_delete='CASCADE')
    outcome = TextField()
    amount = IntegerField()


db.connect()
db.pragma('foreign_keys', 1, permanent=True)
db.create_tables([User, Event, Bet])
